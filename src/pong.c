#include <stdio.h>

int print_area(int width_max, int height_max, int height_player1, int height_player2, int ball_height, int ball_width);
int game();

int main() {
    game();
    return 0;
}

int game() {
    int width = 80, height = 25;
    int height_player1 = height/2;
    int height_player2 = height_player1;
    int ball_height = height/2, ball_width = 2;
    char action;


    print_area(width, height, height_player1, height_player2, ball_height, ball_width);

    while (1) {
        scanf("%c", &action);
        if (action == 'A') {
            if (height_player1 >= 3) {
                height_player1--;
            }
            print_area(width, height, height_player1, height_player2, ball_height, ball_width);
        }

        if (action == 'Z') {
            if (height_player1 <= height - 4) {
                height_player1++;
            }
            print_area(width, height, height_player1, height_player2, ball_height, ball_width);
        }

        if (action == 'K') {
            if (height_player2 >= 3) {
                height_player2--;
            }
            print_area(width, height, height_player1, height_player2, ball_height, ball_width);
        }

        if (action == 'M') {
            if (height_player2 <= height - 4) {
                height_player2++;
            }
            print_area(width, height, height_player1, height_player2, ball_height, ball_width);
        }
    }

    return 0;
}

int print_area(int width_max, int height_max, int height_player1, int height_player2, int ball_height, int ball_width) {
    for (int height = 0; height < height_max; height++) {
        for(int width = 0; width < width_max; width++) {

            if (height == 0 && width == 0) {
                printf("+");
                continue;
            }

            if (height == 0 && width < width_max - 1) {
                printf("-");
                continue;
            }

            if (height == 0 && width == width_max - 1) {
                printf("+\n");
                continue;
            }

            if ((height == height_max - 1) && width == 0) {
                printf("+");
                continue;
            }

            if (height == height_max - 1 && width < width_max - 1) {
                printf("-");
                continue;
            }

            if (height == height_max - 1 && width == width_max - 1) {
                printf("+");
                continue;
            }

            if ((height > 0 || height < height - 1) && width == 0) {
                printf("|");
                continue;
            }

            if ((height > 0 || height < height - 1) && width == width_max - 1) {
                printf("|\n");
                continue;
            }

            if ((width == width_max/2) && height > 0 && height < height_max - 1) {
                printf("|");
                continue;
            }

            // Print player 1
            if ((height == height_player1 - 1) && width == 1) {
                printf("]");
                continue;
            }

            if ((height == height_player1) && width == 1) {
                printf("]");
                continue;
            }

            if ((height == height_player1 + 1) && width == 1) {
                printf("]");
                continue;
            }

            // Print player 2
            if ((height == height_player2 - 1) && width == width_max - 2) {
                printf("[");
                continue;
            }

            if ((height == height_player2) && width == width_max - 2) {
                printf("[");
                continue;
            }

            if ((height == height_player2 + 1) && width == width_max - 2) {
                printf("[");
                continue;
            }

            // Print ball
            if ((height == ball_height) && (width == ball_width)) {
                printf("O");
                continue;
            }

            printf(" ");
        }
    }
    printf("\n");
    return 0;
}
