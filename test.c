#include "data_io.h"

#include <stdio.h>
#include <stdlib.h>

void input(double **data, int *n) {
    if (scanf("%d", n) != 1 && *n > 0) {
        printf("n/a");
        exit(1);
    } else {
        double num;
        int count = 0;
        double *tmp_array = NULL;

        while (count != *n && scanf("%lf", &num) == 1) {
            if (count == 0) {
                tmp_array = (double *)malloc(sizeof(double));
                if (NULL == tmp_array) {
                    printf("n/a");
                    exit(1);
                }
                tmp_array[count] = num;
            } else {
                tmp_array = (double *)malloc(sizeof(double) * count + 1);
                if (NULL == tmp_array) {
                    printf("n/a");
                    exit(1);
                }
                for (int i = 0; i < count; i++) {
                    tmp_array[i] = (*data)[i];
                }
                tmp_array[count] = num;
            }
            count++;
            if (*data != NULL) {
                    free(*data);
                    *data = NULL;
                }
            *data = (double *)malloc(sizeof(double) * count);
            if (NULL == *data) {
                printf("n/a");
                exit(1);
            }
            for (int i = 0; i < count; i++) {
                (*data)[i] = tmp_array[i];
            }
            free(tmp_array);
            tmp_array = NULL;
        }
    }
}

void output(double *data, int n) {
    for (int i = 0; i < n; i++) {
        if (n - 1 == i) {
            printf("%.2lf", data[i]);
        } else {
            printf("%.2lf ", data[i]);
        }
    }
}
